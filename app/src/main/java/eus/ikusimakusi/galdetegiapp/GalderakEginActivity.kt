package eus.ikusimakusi.galdetegiapp

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat

class GalderakEginActivity : AppCompatActivity(), View.OnClickListener {

    private var mOraingoKokalekua : Int = 1
    private var mGalderenZerrenda : ArrayList<Galdera>? = null
    private var mAukeratutakoKokalekua : Int = 0
    private var mErabiltzaileIzena: String? = null
    private var mErantzunZuzenKopurua: Int = 0

    private var progressBar: ProgressBar? = null
    private var tvAurrerabidea: TextView? = null
    private var tvGaldera: TextView? = null
    private var ivIrudia: ImageView? = null

    private var tvAukera1: TextView? = null
    private var tvAukera2: TextView? = null
    private var tvAukera3: TextView? = null
    private var tvAukera4: TextView? = null

    private var btnErantzun: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_galderak_egin)

        mErabiltzaileIzena = intent.getStringExtra(Konstanteak.ERABILTZAILE_IZENA)

        progressBar = findViewById(R.id.pb_aurrerabidea)
        tvAurrerabidea = findViewById(R.id.tv_aurrerabidea)
        tvGaldera = findViewById(R.id.tv_galdera)
        ivIrudia = findViewById(R.id.iv_irudia)

        tvAukera1 = findViewById(R.id.tv_aukera1)
        tvAukera2 = findViewById(R.id.tv_aukera2)
        tvAukera3 = findViewById(R.id.tv_aukera3)
        tvAukera4 = findViewById(R.id.tv_aukera4)
        btnErantzun = findViewById(R.id.btn_erantzun)

        tvAukera1?.setOnClickListener(this)
        tvAukera2?.setOnClickListener(this)
        tvAukera3?.setOnClickListener(this)
        tvAukera4?.setOnClickListener(this)
        btnErantzun?.setOnClickListener(this)

        Log.i("GalderakEginActivity", "mGalderenZerrenda: $mGalderenZerrenda")
        mGalderenZerrenda = Konstanteak.getGalderak()
        Log.i("GalderakEginActivity", "mGalderenZerrenda: $mGalderenZerrenda")
        Log.i("GalderakEginActivity", "Kargatu diren galdera kopurua: ${mGalderenZerrenda?.size}")

        setGaldera()
    }

    private fun setGaldera() {
        erantzunakLehenetsitaBistaratu()
        Log.i("GalderakEginActivity", "mOraingoKokalekua: $mOraingoKokalekua")
        Log.i("GalderakEginActivity", "mGalderenZerrenda: $mGalderenZerrenda")
        val galdera: Galdera = mGalderenZerrenda!![mOraingoKokalekua - 1]
        ivIrudia?.setImageResource(galdera.irudia)

        progressBar?.progress = mOraingoKokalekua
        Log.i("GalderakEginActivity", "Aurrerabidea: $mOraingoKokalekua / ${progressBar?.max}")
        tvAurrerabidea?.text = "$mOraingoKokalekua / ${progressBar?.max}"

        tvGaldera?.text = galdera.galdera
        tvAukera1?.text = galdera.aukera1
        tvAukera2?.text = galdera.aukera2
        tvAukera3?.text = galdera.aukera3
        tvAukera4?.text = galdera.aukera4
        Log.i("GalderakEginActivity", "Galderak ezarri dira")

        if(mOraingoKokalekua == mGalderenZerrenda!!.size){
            btnErantzun?.text = "AMAITU"
        }else{
            btnErantzun?.text = "ERANTZUN"
        }
    }

    private fun erantzunakLehenetsitaBistaratu(){
        val aukerak = ArrayList<TextView>()
        tvAukera1?.let{
            aukerak.add(0, it)
        }
        tvAukera2?.let{
            aukerak.add(1, it)
        }
        tvAukera3?.let{
            aukerak.add(2, it)
        }
        tvAukera4?.let{
            aukerak.add(3, it)
        }
        for(aukera in aukerak){
            aukera.setTextColor(Color.parseColor("#7a8089"))
            aukera.typeface = Typeface.DEFAULT
            aukera.background = ContextCompat.getDrawable(
                this,
                R.drawable.default_option_border_bg
            )
        }
    }

    private fun aukeratutakoErantzunaBistaratu(tv : TextView, aukeratutakoa : Int){
        erantzunakLehenetsitaBistaratu()
        mAukeratutakoKokalekua = aukeratutakoa
        tv.setTextColor(Color.parseColor("#363A43"))
        tv.setTypeface(tv.typeface, Typeface.BOLD)
        tv.background = ContextCompat.getDrawable(
            this,
            R.drawable.selected_option_border_bg
        )

    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.tv_aukera1 -> {
                tvAukera1?.let {
                    aukeratutakoErantzunaBistaratu(it, 1)
                }
            }
            R.id.tv_aukera2 -> {
                tvAukera2?.let {
                    aukeratutakoErantzunaBistaratu(it, 2)
                }
            }
            R.id.tv_aukera3 -> {
                tvAukera3?.let {
                    aukeratutakoErantzunaBistaratu(it, 3)
                }
            }
            R.id.tv_aukera4 -> {
                tvAukera4?.let {
                    aukeratutakoErantzunaBistaratu(it, 4)
                }
            }

            R.id.btn_erantzun -> {
                if(mAukeratutakoKokalekua == 0) {
                    mOraingoKokalekua++
                    Log.i("GalderakEginActivity", "Aurrerabidea + 1")
                    when {
                        mOraingoKokalekua <= mGalderenZerrenda!!.size -> {
                            setGaldera()
                        }
                        else -> {
                            //Toast.makeText(this, "Zorionak!", Toast.LENGTH_LONG).show()
                            val intent = Intent(this, EmaitzaActivity::class.java)
                            intent.putExtra(Konstanteak.ERABILTZAILE_IZENA, mErabiltzaileIzena)
                            intent.putExtra(
                                Konstanteak.ERANTZUN_ZUZEN_KOPURUA,
                                mErantzunZuzenKopurua
                            )
                            intent.putExtra(
                                Konstanteak.GALDERA_KOPURUA,
                                mGalderenZerrenda?.size
                            )
                            startActivity(intent)
                            finish()
                        }
                    }
                }else{
                    val galdera = mGalderenZerrenda?.get(mOraingoKokalekua - 1)
                    if(galdera!!.aukeraZuzena != mAukeratutakoKokalekua) {
                        erantzunaBistaratu(mAukeratutakoKokalekua, R.drawable.wrong_option_border_bg)
                    }else{
                        mErantzunZuzenKopurua++
                    }
                    erantzunaBistaratu(galdera.aukeraZuzena, R.drawable.correct_option_border_bg)

                    if(mOraingoKokalekua == mGalderenZerrenda!!.size){
                        btnErantzun?.text = "AMAITU"
                    }else{
                        btnErantzun?.text = "IKUSI HURRENGOA"
                    }

                    mAukeratutakoKokalekua = 0
                }
            }
        }
    }

    private fun erantzunaBistaratu(erantzuna: Int, drawableView: Int){
        when(erantzuna){
            1 -> {
                tvAukera1?.background = ContextCompat.getDrawable(
                    this,
                    drawableView
                )
            }
            2 -> {
                tvAukera2?.background = ContextCompat.getDrawable(
                    this,
                    drawableView
                )
            }
            3 -> {
                tvAukera3?.background = ContextCompat.getDrawable(
                    this,
                    drawableView
                )
            }
            4 -> {
                tvAukera4?.background = ContextCompat.getDrawable(
                    this,
                    drawableView
                )
            }
        }
    }
}