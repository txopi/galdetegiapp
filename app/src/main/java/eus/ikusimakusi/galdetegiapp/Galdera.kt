package eus.ikusimakusi.galdetegiapp

data class Galdera(
    val id: Int,
    val galdera: String,
    val irudia: Int,
    val aukera1: String,
    val aukera2: String,
    val aukera3: String,
    val aukera4: String,
    val aukeraZuzena: Int
)
