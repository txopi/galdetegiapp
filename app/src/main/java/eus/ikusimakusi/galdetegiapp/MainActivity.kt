package eus.ikusimakusi.galdetegiapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnHasi : Button = findViewById(R.id.btn_hasi)
        val etIzena : EditText = findViewById(R.id.et_izena)
        btnHasi.setOnClickListener {
            if(etIzena.text.isEmpty()){
                Toast.makeText(this, "Sartu zure izena mesedez.", Toast.LENGTH_LONG).show()
            }else{
                val intent = Intent(this, GalderakEginActivity::class.java)
                intent.putExtra(Konstanteak.ERABILTZAILE_IZENA, etIzena.text.toString())
                startActivity(intent)
                finish()
            }
        }

    }
}