package eus.ikusimakusi.galdetegiapp

object Konstanteak {

    const val ERABILTZAILE_IZENA : String = "user_name"
    const val GALDERA_KOPURUA : String = "galdera_kopurua"
    const val ERANTZUN_ZUZEN_KOPURUA : String = "erantzun_zuzen_kopurua"

    fun getGalderak(): ArrayList<Galdera>{
        val galderaZerrenda = ArrayList<Galdera>()

        val galdera1 = Galdera(
            1, "Zenbat bihotz ditu olagarroak?",
            R.drawable.olagarroa_204px,
            "Zero patatero", "Bat", "Bi", "Hiru",
            4
        )
        galderaZerrenda.add(galdera1)

        val galdera2 = Galdera(
            1, "Udazkenean eta neguan biluzik egoten naiz; udaberian eta udan, berriz, arropaz beteta. Nor naiz?",
            R.drawable.urtaroak_204px,
            "Aulkia", "Zuhaitza", "Armairua", "Egutegia",
            2
        )
        galderaZerrenda.add(galdera2)

        val galdera3 = Galdera(
            1, "Nola EZ da esaten agur koreeraz?",
            R.drawable.agur_204px,
            "안녕히 가세요", "안녕히 가세요", "안녕히 자동차", "잘 가요",
            3
        )
        galderaZerrenda.add(galdera3)

        val galdera4 = Galdera(
            1, "Zenbat da biderketa honen emaitza?",
            R.drawable.biderketa_204px,
            "12", "16", "18", "20",
            3
        )
        galderaZerrenda.add(galdera4)

        val galdera5 = Galdera(
            1, "Nola deitzen da Ibaibeko iratxoa?",
            R.drawable.margot_204px,
            "Anet", "Charlot", "Clodet", "Margot",
            4
        )
        galderaZerrenda.add(galdera5)

        val galdera6 = Galdera(
            1, "Zein Pokemon mota da hau?",
            R.drawable.wooper_204px,
            "WOOPER", "PIKACHU", "FURRET", "EELEKTROSS",
            1
        )
        galderaZerrenda.add(galdera6)

        return galderaZerrenda
    }
}