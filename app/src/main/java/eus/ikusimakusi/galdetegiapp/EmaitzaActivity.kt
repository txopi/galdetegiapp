package eus.ikusimakusi.galdetegiapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class EmaitzaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_emaitza)

        val tvZorionak : TextView = findViewById(R.id.tv_zorionak)
        val tvPuntuazioa : TextView = findViewById(R.id.tv_puntuazioa)
        val btnAmaitu : Button = findViewById(R.id.btn_amaitu)

        val mErabiltzaileIzena : String? = intent.getStringExtra(Konstanteak.ERABILTZAILE_IZENA)
        val mErantzunZuzenKopurua : Int? = intent.getIntExtra(Konstanteak.ERANTZUN_ZUZEN_KOPURUA, 0)
        val mGalderaKopurua : Int? = intent.getIntExtra(Konstanteak.GALDERA_KOPURUA, 0)

        tvZorionak.text = "Zorionak $mErabiltzaileIzena!"
        tvPuntuazioa.text = "${mGalderaKopurua}tik $mErantzunZuzenKopurua asmatu dituzu"

        btnAmaitu.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}