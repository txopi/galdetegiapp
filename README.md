## Honi buruz

Proiektu hau **Kotlin** programazio lengoaia erabiliz garatutako probako **Android** aplikazio sinple bat da. GaldetegiApp, galdetegiak egiteko balio du. Erabiltzaileak bere izena sartu ondoren, aplikazioak zenbait galdera egiten dizkio. Amaitzean, lortu duen emaitza erakusten dio.

Aplikazio honen jatorria [QuizApp](https://github.com/tutorialseu/QuizApp) da.

## Pantaila-argazkiak

<div style="display:flex;" >
<img style="margin-right:10px;" src="screenshots/izena.jpg" width="30%" >
<img style="margin-right:10px;" src="screenshots/galdera.jpg" width="30%" >
<img src="screenshots/emaitza.jpg" width="30%" >
</div>
